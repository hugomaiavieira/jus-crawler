package requester

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
)

// RequestError TODO: write doc
type RequestError struct {
	Error string `json:"error"`
}

// GetLawsuitHTMLPage TODO: write docs
func GetLawsuitHTMLPage(url string) ([]byte, error) {
	req, _ := http.NewRequest("GET", url, nil)

	resp, err := httpClient().Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Some problem have occurred. Status code: %d", resp.StatusCode)
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	return body, nil
}

func httpClient() *http.Client {
	cookieJar, _ := cookiejar.New(nil)

	return &http.Client{
		Jar: cookieJar,
	}
}
