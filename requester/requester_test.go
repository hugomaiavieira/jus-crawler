package requester_test

import (
	"errors"
	"testing"

	"com.hugomaiavieira.jusbrasil/crawler/requester"
	. "com.hugomaiavieira.jusbrasil/crawler/test_helpers"
	"gopkg.in/h2non/gock.v1"
)

func TestGetLawsuitHTMLPageHandlingAllRedirectsWithCookies(t *testing.T) {
	defer gock.Off()

	url := "https://www2.tjal.jus.br/cpopg/search.do?someParam=someValue"

	gock.New(url).
		Reply(302).
		SetHeader("Location", url).
		AddHeader("Set-Cookie", "JSESSIONID=stable.cpopg1")

	gock.New(url).
		MatchHeader("Cookie", "JSESSIONID=stable.cpopg1").
		Reply(302).
		SetHeader("Location", url).
		AddHeader("Set-Cookie", "JSESSIONID=FA13E04DCAA98778753BC59C57B09445.cpopg1; Path=/cpopg")

	gock.New(url).
		MatchHeader("Cookie", "JSESSIONID=FA13E04DCAA98778753BC59C57B09445.cpopg1").
		Reply(200).
		BodyString("<html></html>")

	html, _ := requester.GetLawsuitHTMLPage(url)
	Equals(t, []byte("<html></html>"), html)

	Assert(t, gock.IsDone())
}

func TestGetLawsuitHTMLPageWhenUrlIsInvalid(t *testing.T) {
	defer gock.Off()

	_, err := requester.GetLawsuitHTMLPage("invalid-url")
	Assert(t, err != nil)

	Assert(t, gock.IsDone())
}

func TestGetLawsuitHTMLPageWhenFatalError(t *testing.T) {
	defer gock.Off()

	url := "https://www2.tjal.jus.br/cpopg/search.do"

	gock.New(url).
		ReplyError(errors.New("connection refused"))

	_, err := requester.GetLawsuitHTMLPage(url)
	Equals(t, err.Error(), "Get https://www2.tjal.jus.br/cpopg/search.do: connection refused")

	Assert(t, gock.IsDone())
}

func TestGetLawsuitHTMLPageWhenResponseError(t *testing.T) {
	defer gock.Off()

	url := "https://www2.tjal.jus.br/cpopg/search.do?someParam=someValue"

	gock.New(url).
		Reply(502)

	_, err := requester.GetLawsuitHTMLPage(url)
	Equals(t, err.Error(), "Some problem have occurred. Status code: 502")

	Assert(t, gock.IsDone())
}
