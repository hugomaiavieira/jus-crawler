package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"com.hugomaiavieira.jusbrasil/crawler/domain"
	"com.hugomaiavieira.jusbrasil/crawler/parser"
	"com.hugomaiavieira.jusbrasil/crawler/queue"
	"com.hugomaiavieira.jusbrasil/crawler/requester"
	"com.hugomaiavieira.jusbrasil/crawler/urlbuilder"
)

// StandardError TODO: write doc
type StandardError struct {
	Error  string `json:"error"`
	Number string `json:"number"`
}

func main() {
	qs := queue.NewService()
	defer qs.Close()
	qs.StartConsumer(consumeFunc)
}

func consumeFunc(input map[string]interface{}, qs *queue.Service) {
	number := fmt.Sprintf("%v", input["number"])
	result := makeAllRequetsAndAssembleJSON(number)
	qs.PublishMessage(result)
}

func makeAllRequetsAndAssembleJSON(number string) string {
	var result interface{}

	court, err := getCourt(number)

	if err != nil {
		result = StandardError{Error: err.Error(), Number: number}
	} else {
		ch1 := make(chan interface{}, 1)
		ch2 := make(chan interface{}, 1)

		go getLawsuit(number, court, 1, ch1)
		go getLawsuit(number, court, 2, ch2)

		result = domain.LawsuitSearch{
			Number:       number,
			Court:        court,
			FirstDegree:  <-ch1,
			SecondDegree: <-ch2,
		}
	}

	rjson, err := json.Marshal(result)
	if err != nil {
		log.Fatal(err)
	}
	return string(rjson)
}

func getCourt(number string) (string, error) {
	var court string

	switch number[13:16] {
	case "802":
		court = "tjal"
	case "812":
		court = "tjms"
	default:
		return "", errors.New("O número fornecido é inválido")
	}

	return court, nil
}

func getLawsuit(lawsuitID, court string, degree int, ch chan interface{}) {
	var result interface{}
	var err error

	url := urlbuilder.BuildLawsuitURL(lawsuitID, court, degree)
	html, err := requester.GetLawsuitHTMLPage(url)

	switch {
	case court == "tjal" && degree == 1:
		result, err = parser.ParseTjal1(html)
	case court == "tjal" && degree == 2:
		result, err = parser.ParseTjal2(html)
	case court == "tjms" && degree == 1:
		result, err = parser.ParseTjms1(html)
	case court == "tjms" && degree == 2:
		result, err = parser.ParseTjms2(html)
	}

	if err != nil {
		result = requester.RequestError{Error: err.Error()}
	}

	ch <- result
}
