FROM golang:1.13.3-alpine

RUN apk add build-base

WORKDIR /go/src/app

COPY . .

RUN go install
