package domain

// LawsuitSearch TODO: write doc
type LawsuitSearch struct {
	Number       string      `json:"number"`
	Court        string      `json:"court"`
	FirstDegree  interface{} `json:"first_degree"`
	SecondDegree interface{} `json:"second_degree"`
}

// Lawsuit TODO: write doc
type Lawsuit struct {
	Class            string  `json:"class"`
	Area             string  `json:"area"`
	Subject          string  `json:"subject"`
	DistributionDate string  `json:"distribution_date"`
	Judge            string  `json:"judge"`
	ClaimValue       string  `json:"claim_value"`
	Parties          []Party `json:"parties"`
	Moves            []Move  `json:"moves"`
}

// Party TODO: write doc
type Party struct {
	Title   string   `json:"title"`
	Name    string   `json:"name"`
	Lawyers []Lawyer `json:"lawyers"`
}

// Lawyer TODO: write doc
type Lawyer struct {
	Title string `json:"title"`
	Name  string `json:"name"`
}

// Move TODO: write doc
type Move struct {
	Date        string `json:"date"`
	Description string `json:"description"`
}
