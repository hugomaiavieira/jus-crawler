package parser_test

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"com.hugomaiavieira.jusbrasil/crawler/domain"
	"com.hugomaiavieira.jusbrasil/crawler/parser"
	. "com.hugomaiavieira.jusbrasil/crawler/test_helpers"
)

func TestParseTJAL1WithSuccess(t *testing.T) {
	expected := domain.Lawsuit{
		Class:            "Procedimento Ordinário",
		Area:             "Cível",
		Subject:          "Dano Material",
		DistributionDate: "02/05/2018",
		Judge:            "Henrique Gomes de Barros Teixeira",
		ClaimValue:       "R$ 281.178,42",
		Parties: []domain.Party{
			domain.Party{
				Title: "Autor",
				Name:  "José Carlos Cerqueira Souza Filho",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Vinicius Faria de Cerqueira",
					},
				},
			},
			domain.Party{
				Title: "Ré",
				Name:  "Cony Engenharia Ltda.",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Marcus Vinicius Cavalcante Lins Filho",
					},
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Thiago Maia Nobre Rocha",
					},
				},
			},
		},
		Moves: []domain.Move{
			domain.Move{
				Date:        "10/05/2018",
				Description: "Não Concedida a Antecipação de tutela\nDECISÃOTrata-se de ação ordinária de ...",
			},
			domain.Move{
				Date:        "02/05/2018",
				Description: "Conclusos",
			},
			domain.Move{
				Date:        "02/05/2018",
				Description: "Distribuído por Sorteio",
			},
		},
	}

	html := fixtureHTML("tjal1_success_07108025520188020001")
	got, err := parser.ParseTjal1(html)

	Equals(t, nil, err)
	Equals(t, expected.Class, got.Class)
	Equals(t, expected.Area, got.Area)
	Equals(t, expected.Subject, got.Subject)
	Equals(t, expected.DistributionDate, got.DistributionDate)
	Equals(t, expected.Judge, got.Judge)
	Equals(t, expected.ClaimValue, got.ClaimValue)
	Equals(t, expected.Parties, got.Parties)
	Equals(t, expected.Moves, got.Moves)
}

func TestParseTJAL1NotExistent(t *testing.T) {
	html := fixtureHTML("tjal1_not_existent_000000000000008020000")
	got, err := parser.ParseTjal1(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "Não existem informações disponíveis para os parâmetros informados.", err.Error())
}

func TestParseTJAL1WithOnlyMainParty(t *testing.T) {
	expected := []domain.Party{
		domain.Party{
			Title: "Autor",
			Name:  "Tayrone de Noronha Pereira",
			Lawyers: []domain.Lawyer{
				domain.Lawyer{
					Title: "Advogado",
					Name:  "DAVID DA SILVA",
				},
			},
		},
		domain.Party{
			Title:   "Ré",
			Name:    "BV Financeira S/A Crédito, Financiamento e Investimento",
			Lawyers: []domain.Lawyer(nil),
		},
	}

	html := fixtureHTML("tjal1_only_main_court_proceedings_part_07119414220188020001")
	got, _ := parser.ParseTjal1(html)

	Equals(t, expected, got.Parties)
}

func TestParseTJAL2WithSuccess(t *testing.T) {
	expected := domain.Lawsuit{
		Class:            "Precatório",
		Area:             "Cível",
		Subject:          "Precatório",
		DistributionDate: "Precatório/Presidência",
		Judge:            "DES. TUTMÉS AIRAN DE ALBUQUERQUE MELO",
		ClaimValue:       "2 / 0",
		Parties: []domain.Party{
			domain.Party{
				Title:   "Requerente",
				Name:    "Presidente do Tribunal de Justiça do Estado de Alagoas",
				Lawyers: []domain.Lawyer(nil),
			},
			domain.Party{
				Title: "Credor",
				Name:  "Sindicato dos Policiais Civis do Estado de Alagoas - SINDIPOL",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Cláudia Lopes Medeiros",
					},
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Marcos Bernardes de Mello",
					},
				},
			},
			domain.Party{
				Title:   "Requerido",
				Name:    "Tribunal de Justiça do Estado de Alagoas",
				Lawyers: []domain.Lawyer(nil),
			},
			domain.Party{
				Title: "Devedor",
				Name:  "Estado de Alagoas",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Procurador",
						Name:  "Camille Maia Normande Braga",
					},
				},
			},
		},
		Moves: []domain.Move{
			domain.Move{
				Date:        "30/08/2019",
				Description: "Juntada de Documento",
			},
			domain.Move{
				Date:        "14/05/2015",
				Description: "Ato ordinatório praticado\nA T O O R D I N A T Ó R I O / D E S P A C H O 01.",
			},
			domain.Move{
				Date:        "13/04/2011",
				Description: "Remessa ao Gabinete do Presidente",
			},
		},
	}

	html := fixtureHTML("tjal2_success_00049322020118020000")
	got, err := parser.ParseTjal2(html)

	Equals(t, nil, err)
	Equals(t, expected.Class, got.Class)
	Equals(t, expected.Area, got.Area)
	Equals(t, expected.Subject, got.Subject)
	Equals(t, expected.DistributionDate, got.DistributionDate)
	Equals(t, expected.Judge, got.Judge)
	Equals(t, expected.ClaimValue, got.ClaimValue)
	Equals(t, expected.Parties, got.Parties)
	Equals(t, expected.Moves, got.Moves)
}

func TestParseTJAL2NotExistent(t *testing.T) {
	html := fixtureHTML("tjal2_not_existent_999999999999998029999")
	got, err := parser.ParseTjal2(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "Não existem informações disponíveis para os parâmetros informados.", err.Error())
}

func TestParseTJMS1WithSuccess(t *testing.T) {
	expected := domain.Lawsuit{
		Class:            "Procedimento Comum Cível",
		Area:             "Cível",
		Subject:          "Enquadramento",
		DistributionDate: "30/07/2018",
		Judge:            "Zidiel Infantino Coutinho",
		ClaimValue:       "R$ 10.000,00",
		Parties: []domain.Party{
			domain.Party{
				Title: "Autora",
				Name:  "Leidi Silva Ormond Galvão",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Adriana Catelan Skowronski",
					},
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Ana Silvia Pessoa Salgado de Moura",
					},
				},
			},
			domain.Party{
				Title: "Autora",
				Name:  "Melissa Chaves Miranda",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Adriana Catelan Skowronski",
					},
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Ana Silvia Pessoa Salgado de Moura",
					},
				},
			},
			domain.Party{
				Title: "Autora",
				Name:  "Ruzymar Campos de Oliveira",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Adriana Catelan Skowronski",
					},
					domain.Lawyer{
						Title: "Advogada",
						Name:  "Ana Silvia Pessoa Salgado de Moura",
					},
				},
			},
			domain.Party{
				Title: "Réu",
				Name:  "Estado de Mato Grosso do Sul",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "RepreLeg",
						Name:  "Procuradoria Geral do Estado de Mato Grosso do Sul",
					},
				},
			},
		},
		Moves: []domain.Move{
			domain.Move{
				Date:        "25/11/2018",
				Description: "Informação do Sistema\nPJMS - Certidão de realização de consulta de repetiçaõ de ação",
			},
			domain.Move{
				Date:        "30/07/2018",
				Description: "Processo Distribuído por Sorteio",
			},
		},
	}

	html := fixtureHTML("tjms1_success_08219015120188120001")
	got, err := parser.ParseTjms1(html)

	Equals(t, nil, err)
	Equals(t, expected.Class, got.Class)
	Equals(t, expected.Area, got.Area)
	Equals(t, expected.Subject, got.Subject)
	Equals(t, expected.DistributionDate, got.DistributionDate)
	Equals(t, expected.Judge, got.Judge)
	Equals(t, expected.ClaimValue, got.ClaimValue)
	Equals(t, expected.Parties, got.Parties)
	Equals(t, expected.Moves, got.Moves)
}

func TestParseTJMS1NotExistent(t *testing.T) {
	html := fixtureHTML("tjms1_not_existent_999999999999998129999")
	got, err := parser.ParseTjms1(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "Não existem informações disponíveis para os parâmetros informados.", err.Error())
}

func TestParseTJMS1OutOfService(t *testing.T) {
	html := fixtureHTML("tjms1_out_of_service_08219015120188120001")
	got, err := parser.ParseTjms2(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "Não foi possível obter os dados do processo. Por favor tente novamente mais tarde.", err.Error())
}

func TestParseTJMS2WithSuccess(t *testing.T) {
	expected := domain.Lawsuit{
		Class:            "Apelação Cível",
		Area:             "Cível",
		Subject:          "Adicional de Insalubridade",
		DistributionDate: "1ª Câmara Cível",
		Judge:            "DES. DIVONCIR SCHREINER MARAN",
		ClaimValue:       "2.982,18",
		Parties: []domain.Party{
			domain.Party{
				Title: "Apelante",
				Name:  "Adrielli dos Santos Brito",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Rafael Coldibelli Francisco Filho",
					},
					domain.Lawyer{
						Title: "Advogado",
						Name:  "Arthur Andrade Francisco",
					},
				},
			},
			domain.Party{
				Title: "Apelado",
				Name:  "Município Glória de Dourados",
				Lawyers: []domain.Lawyer{
					domain.Lawyer{
						Title: "Proc. Município",
						Name:  "Andressa Alves Garcia Lopes",
					},
					domain.Lawyer{
						Title: "Proc. Município",
						Name:  "Cássia Obregão Ferreira",
					},
				},
			},
		},
		Moves: []domain.Move{
			domain.Move{
				Date:        "29/08/2019",
				Description: "Julgamento Virtual Iniciado",
			},
			domain.Move{
				Date:        "19/08/2019",
				Description: "Recurso Eletrônico Recebido da 1ª Instância\nForo ...",
			},
		},
	}

	html := fixtureHTML("tjms2_success_08000297520188120034")
	got, err := parser.ParseTjms2(html)

	Equals(t, nil, err)
	Equals(t, expected.Class, got.Class)
	Equals(t, expected.Area, got.Area)
	Equals(t, expected.Subject, got.Subject)
	Equals(t, expected.DistributionDate, got.DistributionDate)
	Equals(t, expected.Judge, got.Judge)
	Equals(t, expected.ClaimValue, got.ClaimValue)
	Equals(t, expected.Parties, got.Parties)
	Equals(t, expected.Moves, got.Moves)
}

func TestParseTJMS2Locked(t *testing.T) {
	html := fixtureHTML("tjms2_locked_00004298020178120027")
	got, err := parser.ParseTjms2(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "este processo está protegido por segredo de justiça e requer senha de acesso", err.Error())
}

func TestParseTJMS2NotExistent(t *testing.T) {
	html := fixtureHTML("tjms2_not_existent_000000000000008120000")
	got, err := parser.ParseTjms2(html)

	Equals(t, domain.Lawsuit{}, got)
	Equals(t, "Não existem informações disponíveis para os parâmetros informados.", err.Error())
}

// ------------------------------------
// Test helper functions
// ------------------------------------

func fixtureHTML(name string) []byte {
	html, err := ioutil.ReadFile("./testdata/fixtures/" + name + ".html")
	if err != nil {
		panic(err)
	}
	return html
}

// assert fails the test if the condition is false.
func assert(tb testing.TB, condition bool, msg string, v ...interface{}) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: "+msg+"\033[39m\n\n", append([]interface{}{filepath.Base(file), line}, v...)...)
		tb.FailNow()
	}
}

// ok fails the test if an err is not nil.
func ok(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}
