package parser

import (
	"bytes"
	"errors"
	"log"
	"regexp"
	"strings"

	"github.com/antchfx/htmlquery"
	"golang.org/x/net/html"

	"com.hugomaiavieira.jusbrasil/crawler/domain"
)

// ParseTjal1 TODO: write doc
func ParseTjal1(html []byte) (domain.Lawsuit, error) {
	doc := parseHTML(html)

	if err := checkErrors(doc); err != nil {
		return domain.Lawsuit{}, err
	}

	textNode := htmlquery.Find(doc, "//table[contains(@class, 'secaoFormBody')][@id='']/*text()")[0]
	rawText := htmlquery.InnerText(textNode)

	array := cleanTextAsSlice(rawText)

	return domain.Lawsuit{
		Class:            array[3],
		Area:             strings.Replace(array[4], "Área: ", "", 1),
		Subject:          array[6],
		DistributionDate: getDateFromText(array[10]),
		Judge:            array[15],
		ClaimValue:       array[17],
		Parties:          parseParties(doc),
		Moves:            parseMoves(doc),
	}, nil
}

// ParseTjal2 TODO: write doc
func ParseTjal2(html []byte) (domain.Lawsuit, error) {
	doc := parseHTML(html)

	if err := checkErrors(doc); err != nil {
		return domain.Lawsuit{}, err
	}

	textNode := htmlquery.Find(doc, "//table[contains(@class, 'secaoFormBody')][@id='']/*text()")[0]
	rawText := htmlquery.InnerText(textNode)

	array := cleanTextAsSlice(rawText)

	return domain.Lawsuit{
		Class:            array[4],
		Area:             array[6],
		Subject:          array[8],
		DistributionDate: array[12],
		Judge:            array[14],
		ClaimValue:       array[16],
		Parties:          parseParties(doc),
		Moves:            parseMoves(doc),
	}, nil
}

// ParseTjms1 TODO: write doc
func ParseTjms1(html []byte) (domain.Lawsuit, error) {
	doc := parseHTML(html)

	if err := checkErrors(doc); err != nil {
		return domain.Lawsuit{}, err
	}

	textNode := htmlquery.Find(doc, "//table[contains(@class, 'secaoFormBody')][@id='']/*text()")[0]
	rawText := htmlquery.InnerText(textNode)

	array := cleanTextAsSlice(rawText)

	return domain.Lawsuit{
		Class:            array[3],
		Area:             strings.Replace(array[4], "Área: ", "", 1),
		Subject:          array[6],
		DistributionDate: getDateFromText(array[8]),
		Judge:            array[13],
		ClaimValue:       array[15],
		Parties:          parseParties(doc),
		Moves:            parseMoves(doc),
	}, nil
}

// ParseTjms2 TODO: write doc
func ParseTjms2(html []byte) (domain.Lawsuit, error) {
	doc := parseHTML(html)

	if err := checkErrors(doc); err != nil {
		return domain.Lawsuit{}, err
	}

	textNode := htmlquery.Find(doc, "//table[contains(@class, 'secaoFormBody')][@id='']/*text()")[0]
	rawText := htmlquery.InnerText(textNode)

	array := cleanTextAsSlice(rawText)

	return domain.Lawsuit{
		Class:            array[3],
		Area:             array[5],
		Subject:          array[7],
		DistributionDate: array[11],
		Judge:            array[13],
		ClaimValue:       array[19],
		Parties:          parseParties(doc),
		Moves:            parseMoves(doc),
	}, nil
}

func parseHTML(html []byte) *html.Node {
	htmlReader := bytes.NewReader(html)
	doc, err := htmlquery.Parse(htmlReader)
	failOnError(err, "Failed to parse request response body")

	return doc
}

func checkErrors(doc *html.Node) error {
	if messageNode := htmlquery.FindOne(doc, "//td[@id='mensagemRetorno']"); messageNode != nil {
		message := cleanTextLine(htmlquery.InnerText(messageNode))
		return errors.New(message)
	}

	if htmlquery.FindOne(doc, "//div[@id='popupSenhaProcesso']//input[@id='senhaProcesso']") != nil {
		return errors.New("este processo está protegido por segredo de justiça e requer senha de acesso")
	}

	return nil
}

func parseMoves(doc *html.Node) []domain.Move {
	var moves []domain.Move

	for _, tr := range htmlquery.Find(doc, "//tbody[@id='tabelaTodasMovimentacoes']/tr") {
		tds := htmlquery.Find(tr, "/td")

		move := domain.Move{
			Date:        cleanTextLine(htmlquery.InnerText(tds[0])),
			Description: cleanText(htmlquery.InnerText(tds[2])),
		}

		moves = append(moves, move)
	}

	return moves
}

func parseParties(doc *html.Node) []domain.Party {
	var parts []domain.Party

	trs := htmlquery.Find(doc, "//table[@id='tableTodasPartes']/tbody/tr")
	if len(trs) == 0 {
		trs = htmlquery.Find(doc, "//table[@id='tablePartesPrincipais']/tbody/tr")
	}

	for _, tr := range trs {
		part := domain.Party{}

		for i, td := range htmlquery.Find(tr, "/td") {
			if i == 0 {
				part.Title = cleanTextLine(htmlquery.InnerText(td))
			} else {
				text := htmlquery.InnerText(td)
				array := cleanTextAsSliceSplitComma(text)
				part.Name = cleanTextLine(array[0])

				lawyersInfo := array[1:]

				for i := 0; i < len(lawyersInfo)/2; i++ {
					lawyer := domain.Lawyer{Title: cleanTextLine(lawyersInfo[i*2]), Name: cleanTextLine(lawyersInfo[i*2+1])}
					part.Lawyers = append(part.Lawyers, lawyer)
				}
			}
		}

		parts = append(parts, part)
	}

	return parts
}

func cleanTextAsSlice(text string) []string {
	text = cleanText(text)

	return strings.Split(text, "\n")
}

func cleanText(text string) string {
	spaceRegex := regexp.MustCompile(`(\t| | )+`)
	newLineRegex := regexp.MustCompile(`\s{2,}`)

	text = strings.TrimSpace(text)
	text = spaceRegex.ReplaceAllString(text, " ")
	text = newLineRegex.ReplaceAllString(text, "\n")

	return text
}

func cleanTextLine(text string) string {
	spaceRegex := regexp.MustCompile(`(\s| )+`)

	text = strings.TrimSpace(text)
	text = spaceRegex.ReplaceAllString(text, " ")
	text = strings.Replace(text, ":", "", 1)

	return text
}

func getDateFromText(text string) string {
	dateRegex := regexp.MustCompile(`\d{2}/\d{2}/\d{4}`)

	return dateRegex.FindString(text)
}

func cleanTextAsSliceSplitComma(text string) []string {
	commaRegex := regexp.MustCompile(`:\s`)

	text = cleanText(text)
	text = commaRegex.ReplaceAllString(text, ":\n")

	return strings.Split(text, "\n")
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
