#!/bin/sh

set -e

host="$1"
shift
port="$1"
shift
cmd="$@"

until nc -z $host $port; do
  >&2 echo "Waiting for service $host:$port ..."
  sleep 1
done

>&2 echo "Service $host:$port is up - executing command: $cmd"
exec $cmd
