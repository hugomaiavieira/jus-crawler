# Jusbrasil (crawler)

This is the project responsible for crawling the data from the courts websites.

## Setup, run the apps and tests

Take a look at: https://gitlab.com/hugomaiavieira/jus

## TODO

- Organize the code better
- Write more and better tests
- Remove duplication on parser and `failOnError`.
- Think in a better way to parse the basic data instead of using positional slice indexes.
- Design better the messages, maybe using a data transfer object to define the interface
- Handle errors in a better way and setup Rollbar (or some other tool)
- Deploy to heroku

## Thinking/spiking flow

At first I thought about using a Selenium like tool, but not Selenium because it is too slow. So I found [chromedp](https://github.com/chromedp/chromedp). I was able to make it work after strogloing with some points about submiting the form (I was doing the submit as the examples said, but I only could make it work properly clicking in the butom instead).

Then I realized that the form was sending a GET request, so I decided to change the code to make simple requests. I have strugled with the redirection for a while until I realize that in the browser a cookie was added at the first request and I need to do the same in the code to avoid infinit redirection. So I've added the cookie jar.

I've done some research about parsing the html and using the x/net submodule would be hard. So I've tested the [goquery](https://github.com/PuerkitoBio/goquery) but since the html is awfull, I prefere using xpath via [htmlquery](https://github.com/antchfx/htmlquery).

There are some lawsuits that are available only on the second degree. So we could not assume that it must have the data in the first degree to then look into the second degree. So I decided to do the requests using go routines to improve the performance doing the requests in parallell, since we would have to make the request to both dregrees anyway.

...
