module com.hugomaiavieira.jusbrasil/crawler

go 1.12

require (
	github.com/antchfx/htmlquery v1.0.0
	github.com/antchfx/xpath v1.0.0 // indirect
	github.com/streadway/amqp v0.0.0-20190815230801-eade30b20f1d
	golang.org/x/crypto v0.0.0-20191112222119-e1110fd1c708 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/h2non/gock.v1 v1.0.15
)
