package queue

import (
	"encoding/json"
	"log"
	"os"

	"github.com/streadway/amqp"
)

const resultQueueName string = "jusbrasil/searchResult"
const inputQueueName string = "jusbrasil/searchInput"

// Service TODO: write docs
type Service struct {
	conn        *amqp.Connection
	ch          *amqp.Channel
	inputQueue  amqp.Queue
	resultQueue amqp.Queue
}

type consumeCallback func(map[string]interface{}, *Service)

// Close TODO: write docs
func (qs *Service) Close() {
	qs.conn.Close()
	qs.ch.Close()
}

// NewService TODO: write doc
func NewService() Service {
	url, present := os.LookupEnv("CLOUDAMQP_URL")
	if !present {
		url = "amqp://guest:guest@localhost:5672"
	}
	conn, err := amqp.Dial(url)
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	inputQueue, err := ch.QueueDeclare(
		inputQueueName, // name
		true,           // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	failOnError(err, "Failed to declare a queue")

	resultQueue, err := ch.QueueDeclare(
		resultQueueName, // name
		true,            // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare a queue")

	return Service{
		conn:        conn,
		ch:          ch,
		inputQueue:  inputQueue,
		resultQueue: resultQueue,
	}
}

// StartConsumer TODO: write docs
func (qs *Service) StartConsumer(callback consumeCallback) {
	msgs, err := qs.ch.Consume(
		qs.inputQueue.Name, // queue
		"",                 // consumer
		false,              // auto-ack
		false,              // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			var dat map[string]interface{}
			err := json.Unmarshal(d.Body, &dat)
			failOnError(err, "Failed to parse the input message")
			callback(dat, qs)
			d.Ack(false)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

// PublishMessage TODO: write docs
func (qs *Service) PublishMessage(body string) {
	err := qs.ch.Publish(
		"",                  // exchange
		qs.resultQueue.Name, // routing key
		false,               // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(body),
		})
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent message")
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
