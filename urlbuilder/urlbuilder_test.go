package urlbuilder_test

import (
	"testing"

	. "com.hugomaiavieira.jusbrasil/crawler/test_helpers"
	"com.hugomaiavieira.jusbrasil/crawler/urlbuilder"
)

func TestBuildLawsuitURLForTjal1(t *testing.T) {
	lawsuitID := "07108025520188020001"
	court := "tjal"
	degree := 1

	url := "https://www2.tjal.jus.br/cpopg/search.do?cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&dadosConsulta.valorConsultaNuUnificado=07108025520188020001"
	Equals(t, url, urlbuilder.BuildLawsuitURL(lawsuitID, court, degree))
}

func TestBuildLawsuitURLForTjal2(t *testing.T) {
	lawsuitID := "00049322020118020000"
	court := "tjal"
	degree := 2

	url := "https://www2.tjal.jus.br/cposg5/search.do?&cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&dePesquisaNuUnificado=00049322020118020000"
	Equals(t, url, urlbuilder.BuildLawsuitURL(lawsuitID, court, degree))
}

func TestBuildLawsuitURLForTjms1(t *testing.T) {
	lawsuitID := "08219015120188120001"
	court := "tjms"
	degree := 1

	url := "https://esaj.tjms.jus.br/cpopg5/search.do?cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&dadosConsulta.valorConsultaNuUnificado=08219015120188120001"
	Equals(t, url, urlbuilder.BuildLawsuitURL(lawsuitID, court, degree))
}

func TestBuildLawsuitURLForTjms2(t *testing.T) {
	lawsuitID := "08000297520188120034"
	court := "tjms"
	degree := 2

	url := "https://esaj.tjms.jus.br/cposg5/search.do?cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&dePesquisaNuUnificado=08000297520188120034"
	Equals(t, url, urlbuilder.BuildLawsuitURL(lawsuitID, court, degree))
}
