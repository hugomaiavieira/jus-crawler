package urlbuilder

import "fmt"

var urls = map[string]map[int]string{
	"tjal": {
		1: "https://www2.tjal.jus.br/cpopg/search.do?cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&dadosConsulta.valorConsultaNuUnificado=%s",
		2: "https://www2.tjal.jus.br/cposg5/search.do?&cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&dePesquisaNuUnificado=%s",
	},
	"tjms": {
		1: "https://esaj.tjms.jus.br/cpopg5/search.do?cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&dadosConsulta.valorConsultaNuUnificado=%s",
		2: "https://esaj.tjms.jus.br/cposg5/search.do?cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&dePesquisaNuUnificado=%s",
	},
}

// BuildLawsuitURL TODO: write docs
func BuildLawsuitURL(lawsuitID, court string, degree int) string {
	return fmt.Sprintf(urls[court][degree], lawsuitID)
}
